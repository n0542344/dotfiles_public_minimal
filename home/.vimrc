" """ """ """ """ """ """ """ """ " --------------------------------------------
" """  ! manually  created !  """ "
" """        dotfiles         """ "
" """       2020-12-17        """ "

" """ """ .vimrc ===============================================================


" """ use local config file ----------------------------------------------------

if filereadable($HOME . "/.vimrc.local")
  source        $HOME/.vimrc.local
endif



" """ set vi-compatible --------------------------------------------------------

set showmode                            " show mode you are presently in

set showcmd                             " show what you are typing as a command

set shell=sh                            " set shell

set report=0                            " report back on all changes

set showmatch                           " show matching bracket

set ignorecase                          " Ignoring case

set noerrorbells                        " No error bells please


" """ set vi-nocompatible ------------------------------------------------------

set nocompatible

let mapleader='\'                       " define leader-key
let maplocalleader='\'                  " all my macros start with \ (default)

set encoding=utf-8                      " define standard file encoding

set ttyfast                             " set a fast terminal


set fileformats=unix,dos                " support all three newline formats

set path+=**                            " use a fuzzy search when searching for files



" """ generate backups, temporary files and undos ------------------------------

if !isdirectory($HOME . "/.vim/tmp/swap")
    call  mkdir($HOME . "/.vim/tmp/swap", "p", 0700)
endif
set directory=~/.vim/tmp/swap//


if !isdirectory($HOME . "/.vim/tmp/backup")
    call  mkdir($HOME . "/.vim/tmp/backup", "p", 0700)
endif
set backup
set backupdir=~/.vim/tmp/backup//


if !isdirectory($HOME . "/.vim/tmp/undo")
    call  mkdir($HOME . "/.vim/tmp/undo", "p", 0700)
endif
set undofile
set undodir=~/.vim/tmp/undo//
set undolevels=1000


if !isdirectory($HOME . "/.vim/tmp/viminfo")
    call  mkdir($HOME . "/.vim/tmp/viminfo", "p", 0700)
endif
set viminfo=%,<1000,'10,/20,:100,h,f0,n~/.vim/tmp/viminfo/.viminfo.vim



" """ vim-behaviour ------------------------------------------------------------

set shortmess=atI                       " shorten messages and don't show intro

set hidden                              " keep not currently displayed buffers in memory
set history=200                         " keep more commands in memory

set updatecount=100                     " switch every 100 chars

set more                                " use Unix-style more prompt

set autoread                            " watch for file changes
set noautowrite                         " don't automagically write on :next




" """ define fold-method for vim-files -----------------------------------------

set foldenable                          " show folds by default
set foldcolumn=3                        " see the width of the column to display folds on the left
set foldmethod=marker                   " fold based on markers ( by default {{{ .... }}} )
let g:markdown_folding = 1              " automatically enable folding when displaying markdown documents





" """ automatically detect filetype and colors syntax accordingly --------------
" """ use the default vim color syntax settings
" """ 'syntax on'  " overwrites the own color syntax

filetype on                             " Enable filetype detection
filetype indent on                      " Enable filetype-specific indenting
filetype plugin on                      " Enable filetype-specific plugins




" """ search setup -------------------------------------------------------------

set incsearch                           " Incremental searching
set hlsearch                            " Highlighting while searching
"set nohlsearch                          " turn off highlighting for searched expressions
set matchtime=2                         " blink matching chars for .x seconds


set diffopt=filler,iwhite               " ignore all whitespace and sync

set smartcase                           " when searching for lower case, also upper case is found
                                        " when searching for upper case, only upper case is found




" """ tab completion -----------------------------------------------------------

set wildmenu                            " turn on wild menu (using <Tab> at the command line)
set wildmode=list,longest,full          " set wildmenu to list choice

set complete=.,w,b,u,U,t,i,d            " do lots of scanning on tab completion



" """  set colors --------------------------------------------------------------

set background=dark                     " not needed if a colorscheme uses this

if has('syntax')
    syntax on
    " Remember that rxvt-unicode has 88 colors by default; enable this only if
    " you are using the 256-color patch
    if &term == 'rxvt-unicode'
        set t_Co=256
    endif

    if &t_Co == 256
      try
        colorscheme ron
        silent! colorscheme elflord
      catch
        colorscheme default
        silent! colorscheme desert
      endtry
    endif
endif



" """ general appearance -------------------------------------------------------

set visualbell                          " visual signal in case of errors

set nolazyredraw


" """ statusline options -------------------------------------------------------

set cmdheight=1                         " command line hight reduced to one line

set laststatus=2                        " always display the statusbar

set statusline=                         " create empty statusline
set statusline+=%#EndOfBuffer#          " color

set statusline+=[\                      " spacer
set statusline+=%#ModeMsg#              " color
set statusline+=B:\ %n                  " buffer number
set statusline+=%#EndOfBuffer#          " color

set statusline+=\ \/\                   " spacer

set statusline+=%#Title#                " color
set statusline+=%.35t                   " path to the file (using a fraction of max. 0.xx of space)
set statusline+=%#EndOfBuffer#          " color
set statusline+=\ ]                     " spacer

set statusline+=[\                      " spacer
set statusline+=%#Directory#            " color
set statusline+=FT:\ %Y                 " filetype flag, text is "[vim]"
set statusline+=%#EndOfBuffer#          " color
set statusline+=\ ]                     " spacer

set statusline+=[\                      " spacer
set statusline+=%#MoreMsg#              " color
set statusline+=%M                      " modified flag, text is "[+]"
set statusline+=%#EndOfBuffer#          " color

set statusline+=\ \|\                   " spacer

set statusline+=%#ModeMsg#              " color
set statusline+=%R                      " readonly flag, text is "[RO]"
set statusline+=%#EndOfBuffer#          " color
set statusline+=\ ]                     " spacer


set statusline+=%=                      " switch to the right side


set statusline+=[\                      " spacer
set statusline+=%#StorageClass#         " color
set statusline+=L:\ %v\ \-\ %l/%L\ (%p%%) " line number / total line count (percent file)
set statusline+=%#EndOfBuffer#          " color
set statusline+=\ ]                     " spacer



" """ whitespace ---------------------------------------------------------------

set expandtab                           " expand tab to insert spaces instead of tabstops
set smarttab                            " when using tabstops add as many spaces until reaching a multiple of shiftwidth

set tabstop=4                           " tab results in 4 spaces
set softtabstop=4                       " set virtual tab stop (compat for 4-wide tabs)
set shiftwidth=2                        " define number of spaces that make a tabstop

set backspace=2                         " equivalent to: 'set backspace=indent,eol,start'

set autoindent                          " automatic indentation
set smartindent                         " try to be smart about indenting (C-style)

set shiftround                          " always round indents to multiple of shiftwidth
set copyindent                          " use existing indents for new indents
set preserveindent                      " save as much indent structure as possible

set nolist                              " necessary for wrapping



" """ help orientation within file ---------------------------------------------

set number                              " add line number information and make it relative to the cursor position
                                        " could be abbreviated using set nu
set relativenumber                      " displays current line number, the rest ist relative to that line

set ruler                               " add information on cursor position
set cursorline                          " add horizontal line mark on current cursor position
set cursorcolumn                        " add vertical line mark on current cursor position

set scrolloff=5                         " keep at least 5 lines above/below
set sidescrolloff=5                     " keep at least 5 lines left/right

set splitbelow                          " start splitscreen below
set splitright                          " start splitscreen to the right



" """ text formating options ---------------------------------------------------

set linebreak

"set textwidth=80                        " predefine textwidth
set textwidth=0                         " prevent Vim from automatically inserting line breaks
set wrap                                " soft wrap long lines
set wrapmargin=0                        " only used if textwidth=0

set showbreak=>\                        " show string indicating a linebreak

set formatoptions=tcrql                 " t - autowrap to textwidth
                                        " c - autowrap comments to textwidth
                                        " r - autoinsert comment leader with <Enter>
                                        " q - allow formatting of comments with :gq
                                        " l - don't format already long lines

set nostartofline                       " Stable cursor position

set completeopt=menu,longest,preview    " more autocomplete <Ctrl>-P options


