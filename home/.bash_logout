# ### ### ### ### ### ### ### ### # --------------------------------------------
# ###  ! manually  created !  ### #
# ###        dotfiles         ### #
# ###       2019-03-16        ### #

# ### ### .bash_logout: things to do when you log off your system ==============



if [ "$SHLVL" = 1 ]; then
  # clear console
  if [ -x "$(command -v bash)" ]; then
    [ -x /usr/bin/clear_console ]       && /usr/bin/clear_console -q;
  fi
fi

