# ### ### ### ### ### ### ### ### # --------------------------------------------
# ###  ! manually  created !  ### #
# ###        dotfiles         ### #
# ###       2020-11-04        ### #

# ### ### .bashrc: things to do when bash is started interactively =============



# ### site-specific configurations ---------------------------------------------
# dont forget to create that file

if [ -f "$HOME/.bashrc.local" ]; then
      . "$HOME/.bashrc.local"
    else
  touch "$HOME/.bashrc.local"
fi

# copy site-specific things into ~/.bashrc.local, e.g.:
#if       [ -d "$HOME/.local/bin" ]; then
#  export PATH="$HOME/.local/bin/":$PATH
#fi



# ### history ------------------------------------------------------------------

export HISTCONTROL=erasedups:ignoredups:ignorespace
export HISTIGNORE="&:[ ]*:exit:cd:cd *:pwd:exit:ls:ls *:jobs:bg *:fg *:history:clear:date:* --help"

export HISTSIZE=
export HISTFILESIZE=
export HISTFILE="$HOME/.bash_history"

shopt -s histappend
export PROMPT_COMMAND="history -a; history -c; history -r"

HISTTIMEFORMAT='%F %T: '



# ### export standard software -------------------------------------------------

export EDITOR=vim

export PAGER=less



# ### change language settings -------------------------------------------------

export        LANG='en_US.UTF-8'

export      LC_ALL="$LANG"
export  LC_COLLATE="$LANG"
export    LC_CTYPE="$LANG"
export LC_MESSAGES="$LANG"
export  LC_NUMERIC="$LANG"
export     LC_TIME="$LANG"

export LC_MONETARY='de_DE.UTF-8'



# ### security settings --------------------------------------------------------

umask 077

export GPG_TTY=$(tty)



# ### aliases ------------------------------------------------------------------

# CAVE: this overwrites the original commands!

alias    rm='command rm -iv'
alias    mv='command mv -iv'
alias    cp='command cp -iv'


alias    ll='command ls -l --human-readable'        # abbreviated ls -l -h
alias    la='command ls --almost-all'               # abbreviated ls -A
alias    lF='command ls -C --classify'              # abbreviated ls -C -F
alias    l.='command ls --directory .*'             # abbreviated ls -d .*


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then

    test -r "$HOME/.dircolors" && eval "$(dircolors -b $HOME/.dircolors)" || eval "$(dircolors -b)"

    alias   dir='command   dir --color=auto'
    alias  cdir='command   dir --color=auto --human-readable --group-directories-first'

    alias  vdir='command  vdir --color=auto'
    alias cvdir='command  vdir --color=auto --human-readable --group-directories-first'

    alias  diff='command  diff --color=auto'

    alias  grep='command  grep --color=auto'
    alias fgrep='command fgrep --color=auto'
    alias egrep='command egrep --color=auto'
else
    alias  cdir='command   dir              --human-readable --group-directories-first'
    alias cvdir='command  vdir              --human-readable --group-directories-first'
fi



# ### find processes using ps and grep -----------------------------------------
alias       psg='command ps -ef | command grep -i --color=auto'


# only when the used shell is bash (test $BASH)

if [ -n "$BASH" ]; then
    alias   src='command . "$HOME/.bashrc"'
    alias   cls='command . "$HOME/.bashrc"; command clear'
    # alternative: \clear uses the command, not the alias
fi



# ### activate bash-completion -------------------------------------------------

if ! shopt -oq posix; then
    if [ -f /etc/profile.d/bash_completion.sh ]; then
        . /etc/profile.d/bash_completion.sh
    elif [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi



# ### colors -------------------------------------------------------------------

export LESS_TERMCAP_mb=$'\E[1;31m'              # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'              # begin blink
export LESS_TERMCAP_me=$'\E[0m'                 # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m'          # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'                 # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'              # begin underline
export LESS_TERMCAP_ue=$'\E[0m'                 # reset underline


# set a color prompt (non-color, unless we know we "want" color)
case "$TERM" in
    *-color|*-256color) COLOR_PROMPT=yes;;
esac


if tput setaf 1 &> /dev/null; then
    tput sgr0; # reset colors
           blink="$(tput     blink)";
            bold="$(tput      bold)";
          normal="$(tput      sgr0)";
         reverse="$(tput       rev)";
           black="$(tput setaf   0)";
             red="$(tput setaf   1)";
           green="$(tput setaf   2)";
          orange="$(tput setaf   3)";
            blue="$(tput setaf   4)";
          purple="$(tput setaf   5)";
            aqua="$(tput setaf   6)";
           white="$(tput setaf   7)";
      lightgreen="$(tput setaf  10)";    # non-standard colors! alternatives: 40, 46, 119
     lightyellow="$(tput setaf  11)";    # non-standard colors! alternatives: 40, 46, 119
else
            bold=`echo -en "\e[1m"`
       underline=`echo -en "\e[4m"`
             dim=`echo -en "\e[2m"`
   strickthrough=`echo -en "\e[9m"`
           blink=`echo -en "\e[5m"`
         reverse=`echo -en "\e[7m"`
          hidden=`echo -en "\e[8m"`
          normal=`echo -en "\e[0m"`
           black=`echo -en "\e[30m"`
             red=`echo -en "\e[31m"`
           green=`echo -en "\e[32m"`
          orange=`echo -en "\e[33m"`
            blue=`echo -en "\e[34m"`
          purple=`echo -en "\e[35m"`
            aqua=`echo -en "\e[36m"`
            grey=`echo -en "\e[37m"`
        darkgrey=`echo -en "\e[90m"`
        lightred=`echo -en "\e[91m"`
      lightgreen=`echo -en "\e[92m"`
     lightyellow=`echo -en "\e[93m"`
       lightblue=`echo -en "\e[94m"`
     lightpurple=`echo -en "\e[95m"`
       lightaqua=`echo -en "\e[96m"`
           white=`echo -en "\e[97m"`
         default=`echo -en "\e[39m"`
           BLACK=`echo -en "\e[40m"`
             RED=`echo -en "\e[41m"`
           GREEN=`echo -en "\e[42m"`
          ORANGE=`echo -en "\e[43m"`
            BLUE=`echo -en "\e[44m"`
          PURPLE=`echo -en "\e[45m"`
            AQUA=`echo -en "\e[46m"`
            GREY=`echo -en "\e[47m"`
        DARKGREY=`echo -en "\e[100m"`
        LIGHTRED=`echo -en "\e[101m"`
      LIGHTGREEN=`echo -en "\e[102m"`
     LIGHTYELLOW=`echo -en "\e[103m"`
       LIGHTBLUE=`echo -en "\e[104m"`
     LIGHTPURPLE=`echo -en "\e[105m"`
       LIGHTAQUA=`echo -en "\e[106m"`
           WHITE=`echo -en "\e[107m"`
         DEFAULT=`echo -en "\e[49m"`
fi


# Highlight the user name when logged in as root.
if [[ "${USER}" == "root" ]]; then
    COLOR_USER="${red}${blink}${reverse}";
else
    COLOR_USER="${green}";
fi


# Highlight the hostname when connected via SSH.
if [[ "${SSH_TTY}" ]]; then
    COLOR_HOST="${red}${bold}${reverse}";
else
    COLOR_HOST="${orange}";
fi



# ### set prompt (use color only if available) ---------------------------------

if [ "$COLOR_PROMPT"=yes ]; then
    PS1=' ';
    PS1+='\[${normal}\]\n ';
    PS1+='\[${COLOR_USER}${bold}\]\u';      # alternative to ${green}
    PS1+='\[${normal}\] @ ';
    PS1+='\[${COLOR_HOST}${bold}\]\h';      # alternative to ${orange}
    PS1+='\[${normal}\] using ';
    PS1+='\[${purple}${bold}\]$0';
    PS1+='\[${normal}\] on ';
    PS1+='\[${blue}\]$TERM';
    PS1+='\[${normal}\] @ ';
    PS1+='\[${aqua}${bold}\]\w';
    PS1+='\[${normal}\]:\n ';
    PS1+='\[${normal}\]\$ ';
else
    PS1='\u @ \h : \w \$ '
fi


